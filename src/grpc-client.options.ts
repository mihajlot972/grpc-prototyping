import { ClientOptions, Transport } from '@nestjs/microservices';
import { join } from 'path';

export const grpcClientOptions: ClientOptions = {
  transport: Transport.GRPC,
  options: {
    package:[ 'hero', 'helloworld'], // ['hero', 'hero2']
    protoPath: [join(__dirname, './hero/hero.proto'), join(__dirname, './hero/helloworld.proto')], // ['./hero/hero.proto', './hero/hero2.proto']
  },
};