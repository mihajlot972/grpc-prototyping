import { Controller, Get, Inject, OnModuleInit, Param } from '@nestjs/common';
import {
  ClientGrpc,
  GrpcMethod,
  GrpcStreamMethod,
} from '@nestjs/microservices';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { toArray } from 'rxjs/operators';
import { HeroById } from './interfaces/hero-by-id.interface';
import { Hero } from './interfaces/hero.interface';

interface HeroService {
  findOne(data: HeroById): Observable<Hero>;
  findMany(upstream: Observable<HeroById>): Observable<Hero>;
}

interface GreetingService {
  sayHelloAgain(helloRequest: any): Observable<any>
}

@Controller('hero')
export class HeroController implements OnModuleInit {
  private readonly items: Hero[] = [
    { id: 1, name: 'John' },
    { id: 2, name: 'Doe' },
  ];
  private heroService: HeroService;
  private greetingService: GreetingService

  constructor(
    // Inject gRPC Client
    @Inject('HERO_PACKAGE') private readonly client: ClientGrpc
  ) { }

  onModuleInit() {
    this.heroService = this.client.getService<HeroService>('HeroService');
  }

  @GrpcMethod('Greeter', 'SayHelloAgain')
  sayHelloAgain(helloRequest: any) {
    console.log('request from cl: ', helloRequest)
    return {message: "This is something we calculated and returning you new value", age: 2}
  }

  @GrpcMethod('HeroService', 'FindOne')
  findOne(data: HeroById): Hero {
    return this.items.find(({ id }) => id === data.id);
  }

  @GrpcStreamMethod('HeroService')
  findMany(data$: Observable<HeroById>): Observable<Hero> {
    const hero$ = new Subject<Hero>();

    const onNext = (heroById: HeroById) => {
      const item = this.items.find(({ id }) => id === heroById.id);
      hero$.next(item);
    };
    const onComplete = () => hero$.complete();
    data$.subscribe({ next: onNext, complete: onComplete });

    return hero$.asObservable();
  }

  getHero(): Observable<any> {
    return this.heroService.findOne({ id: 1 });
  }
}