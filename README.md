## How to use project:

**You can use this project only as a server for now**

Run `npm run build` to generate dist folder for NestJS, it is supposed to copy all files including .proto files. If that doesn't happen we need to manually copy proto service we want to use and place it under `dist/hero` folder, then: 

1. You need to install client packet `grpcc` globally
> npm install -g grpcc

2. Navigate to the project root in terminal and if you want to use `heroservice` type in 

> grpcc -i --proto ./src/hero/hero.proto --address localhost:5000

3. This command will open client session, in which you can call methods defined in .proto files

> client.getHeroById({ id: 1 }, printReply) - returns hero with id 1

To run server/client services from another languages we can test with their official examples https://github.com/grpc/grpc/tree/master/examples
Running python server (greeter_server.py) and triggering it with node client (greeter_client.js) works perfectly fine.